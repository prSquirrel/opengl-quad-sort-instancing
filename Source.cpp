#pragma comment(lib, "opengl32.lib")

// GLEW
#include <gl/glew.h>

// Window manager
#include <GLFW/glfw3.h>

// STD
#include <vector>
#include <iostream>
#include <algorithm>

#include "BigPixels.hpp"


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

GLuint screenWidth = 640, screenHeight = 640;
double aspectRatio() { return (double)screenHeight / screenWidth; }

using namespace std;

template< typename T >
void quickSort(T arr[], int left, int right) {
	int i = left, j = right;
	int tmp;
	int pivot = arr[(left + right) / 2];

	while (i <= j) {
		while (arr[i] < pivot) {
			i++;
		}
		while (arr[j] > pivot) {
			j--;
		}
		if (i <= j) {
			swap(arr[i], arr[j]);
			i++;
			j--;
		}
	}

	if (left < j) {
		quickSort(arr, left, j);
	}
	if (i < right) {
		quickSort(arr, i, right);
	}
}
int main(int argc, char **argv) {
	//  Connect to the windowing system + create a window
	//  with the specified dimensions and position
	//  + set the display mode + specify the window title.
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Pixel sort", nullptr, nullptr);
	glfwMakeContextCurrent(window);
	
	// Set callbacks
	glfwSetKeyCallback(window, key_callback);

	// Init GLEW
	glewExperimental = GL_TRUE;
	glewInit();

	// Define viewport
	glViewport(0, 0, aspectRatio() * screenWidth, screenHeight);
	
	BigPixels scene(10000);

	// fill it with random colors
	for (size_t i = 0; i < 10000; i++) {
		float r = (float)rand() / (RAND_MAX - 1);
		float g = (float)rand() / (RAND_MAX - 1);
		float b = (float)rand() / (RAND_MAX - 1);

		scene.colors[0 * 10000 + i] = r;
		scene.colors[1 * 10000 + i] = 0.0f;
		scene.colors[2 * 10000 + i] = 0.0f;
	}
//	scene.draw(window);
	//while (!glfwWindowShouldClose(window)) {
	//	glfwPollEvents();
	//	scene.draw(window);
	//}


	int counter = 0;
	sort(scene.colors.begin(), scene.colors.begin() + 10000, [&](const float& a, const float& b) {
		if (counter % 100 == 0 && !glfwWindowShouldClose(window)) {
			glfwPollEvents();
			scene.draw(window);
		}
		counter++;
		return a < b;
	});

	cin.get();

	glfwTerminate();
	return 0;
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}
