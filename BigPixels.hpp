#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>


class BigPixels {
public:
                     BigPixels ( const size_t squares );
	   void          draw ( GLFWwindow* windowContext );
std::vector<float>	 colors;

private:
	 size_t          squares;

	 GLuint			 shaderID;

	   void          initOffsets(void);

	   void          initInstanced(void);
	 GLuint			 instanceVBO;
	 GLuint			 quadVAO;

	   void          initColorData(void);
     GLuint			 colorsTBO;
//std::vector<float>   colors;
	 GLuint          colorsTBO_TEX;
	  GLint			 u_colorsTBO_TEX;
};
