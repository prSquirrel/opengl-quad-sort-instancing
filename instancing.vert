#version 330 core
// position and offset for each instance
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 offset;

// pass color to fragment shader
out vec3 fColor;

// globally accessible texture
// which is being used as a buffer
uniform samplerBuffer u_colorsTBO_TEX;

void main() {
	vec2 pos = position;
	gl_Position = vec4(pos + offset, 0.0f, 1.0f);


	// our texture can only hold RED values
	// therefore, if we store RGB
	// texture is 3x the usual size
	int totalColors = textureSize(u_colorsTBO_TEX) / 3;

	float r = texelFetch(u_colorsTBO_TEX, 0 * totalColors + gl_InstanceID).r;
	float g = texelFetch(u_colorsTBO_TEX, 1 * totalColors + gl_InstanceID).r;
	float b = texelFetch(u_colorsTBO_TEX, 2 * totalColors + gl_InstanceID).r;

	// pass to fragment shader
	fColor = vec3(r, g, b);
}