// GLEW
#include <gl/glew.h>

// Window manager
#include <GLFW/glfw3.h>

// GLM Mathematics 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// STD
#include <vector>
#include <algorithm>

#include "Shader.hpp"
#include "BigPixels.hpp"


BigPixels::BigPixels(const size_t squares): squares(squares) {
	// Load and compile shaders
	this->shaderID = LoadShaders("instancing.vert", "instancing.frag");

	initOffsets();
	initInstanced();
	initColorData();
}

void BigPixels::initOffsets() {
	// create squares and define their offsets
	// (by translation vector)
	std::vector<glm::vec2> translations(squares);
	{
		int index = 0;
		GLfloat offset = 0.05f;
		for (GLint y = -100; y < 100; y += 2) {
			for (GLint x = -100; x < 100; x += 2) {
				glm::vec2 translation;
				translation.x = (GLfloat)x / 100.0f + offset;
				translation.y = (GLfloat)y / 100.0f + offset;

				translations[index++] = translation;
			}
		}
	}
	// request buffer
	glGenBuffers(1, &this->instanceVBO);
	glBindBuffer(GL_ARRAY_BUFFER, this->instanceVBO);
	// copy array of offsets to new buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * translations.size(), &translations.data()[0], GL_STATIC_DRAW);
	// unbind buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void BigPixels::initInstanced() {
	// a quad(rectangle) is two triangles placed together
	GLfloat quadVertices[] = {
		// Positions   
		-0.05f,  0.05f, //
		0.05f, -0.05f, // first triangle
		-0.05f, -0.05f, //

		-0.05f,  0.05f, //
		0.05f, -0.05f, // second triangle
		0.05f,  0.05f  //
	};

	glGenVertexArrays(1, &this->quadVAO);
	glBindVertexArray(this->quadVAO);

	GLuint quadVBO;
	glGenBuffers(1, &quadVBO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	//				   id,size,     type,    norm.,    length of stride, starting index of this data in array
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);

	// set instance data
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, this->instanceVBO);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glVertexAttribDivisor(1, 1);
	glBindVertexArray(0);
}

void BigPixels::initColorData() {
	// prepare texture buffer
	glGenBuffers(1, &this->colorsTBO);

	// prepare texture
	glGenTextures(1, &this->colorsTBO_TEX);

	// get unique location of variable in vertex shader
	// so that we can transfer our newly-forged texture
	this->u_colorsTBO_TEX = glGetUniformLocation(this->shaderID, "u_colorsTBO_TEX");

	// RGB values for each square
	this->colors.resize(squares * 3, 1.0f);
}

void BigPixels::draw(GLFWwindow* windowContext) {
	// Clear buffers
	glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);



	// bind texture buffer
	glBindBuffer(GL_TEXTURE_BUFFER, this->colorsTBO);

	// copy color data to texture buffer
	glBufferData(GL_TEXTURE_BUFFER, this->colors.size() * sizeof(float), this->colors.data(), GL_STATIC_DRAW);
	// unbind texture buffer
	glBindBuffer(GL_TEXTURE_BUFFER, 0);

	glActiveTexture(GL_TEXTURE0);
	// bind texture to buffer
	glBindTexture(GL_TEXTURE_BUFFER, this->colorsTBO_TEX);
	// copy data from previous buffer to texture
	glTexBuffer(GL_TEXTURE_BUFFER, GL_R32F, this->colorsTBO);
	// transfer 1D texture to vertex shader
	glUniform1i(this->u_colorsTBO_TEX, 0);

	// draw instanced squares
	glUseProgram(this->shaderID);

	glBindVertexArray(this->quadVAO);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, this->squares);
	glBindVertexArray(0);

	// swap the buffers
	glfwSwapBuffers(windowContext);
}